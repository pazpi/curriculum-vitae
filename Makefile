all: ita eng

ita: cv_Pasetto_Davide.tex
	sed -i 's/ITAfalse/ITAtrue/' cv_Pasetto_Davide.tex
	xelatex cv_Pasetto_Davide.tex
	mv cv_Pasetto_Davide.pdf cv_Pasetto_Davide_ITA.pdf

eng: cv_Pasetto_Davide.tex
	sed -i 's/ITAtrue/ITAfalse/' cv_Pasetto_Davide.tex
	xelatex cv_Pasetto_Davide.tex
	mv cv_Pasetto_Davide.pdf cv_Pasetto_Davide_ENG.pdf
	sed -i 's/ITAfalse/ITAtrue/' cv_Pasetto_Davide.tex
